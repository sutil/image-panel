import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class AppServiceService {

  constructor(private http: HttpClient) {

  }

  buscaImagens() :Observable<any[]>{
    let imgs: string[] = [];
    return this.http.get<any[]>("http://paineltv.ieqmaringa.com.br/wp-json/wp/v2/media?_embed");
  }

}
