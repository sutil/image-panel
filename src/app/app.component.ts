import { AppServiceService } from './app-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  index:number = 0;

  urlImg = "./assets/img/logo.png";
  
  urls = [];

  constructor(private service: AppServiceService){

  }

  ngOnInit(){
    console.log("buscando imagens");
    this.service.buscaImagens().subscribe(
      (data => {
        this.urls = data.map(el => el.guid.rendered);
        this.iniciaExibicao();
      })
    );
  }

  iniciaExibicao(){
    setInterval(() => {
      if(this.index == this.urls.length -1){
        this.index = 0;
      }
      else{
        this.index ++;
      }
      this.urlImg = this.urls[this.index];
    }, 6000);
  }
}
